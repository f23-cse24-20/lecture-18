#ifndef UCM_STRINGS_H
#define UCM_STRINGS_H

#include <string>
#include <iostream>

// Welcome to UC Merced => Welcome

// longestWord = "Welcome"
// word = "UC"


bool isPunctuation(char letter) {
    if (letter == ' ' || letter == ',' || letter == '.') {
        return true;
    }
    return false;
}

std::string longestWord(std::string text) {
    std::string longestWord = "";

    std::string word = "";
    for (int i = 0; i < text.length(); i++) {
        if (isPunctuation(text[i])) {
            if (word.length() >= longestWord.length()) {
                longestWord = word;
            }
            word = "";
        } else {
            word += text[i];
        }
    }

    if (word.length() >= longestWord.length()) {
        longestWord = word;
    }

    return longestWord;
}

#endif