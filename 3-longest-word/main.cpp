#include <iostream>
#include "ucm_strings.h"
using namespace std;

int main() {

    /*
        Create a function that finds the longest word in a string.

        Example:
        Welcome to UC Merced => Welcome
    */

    string text = "Hello, bobby";
    string word = longestWord(text);

    cout << "Longest word: " << word << endl;

    return 0;
}
