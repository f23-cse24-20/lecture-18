#ifndef UCM_STRINGS_H
#define UCM_STRINGS_H

#include <iostream>
#include <string>

// Welcome to UC Merced => Wlcm t C Mrcd

// result = "W"

bool isVowel(char letter) {
    letter = toupper(letter);

    if (letter == 'A' || letter == 'E' || letter == 'I' || letter == 'O' || letter == 'U') {
        return true;
    }
    
    return false;
}

std::string removeVowels(std::string text) {
    std::string result = "";

    for (int i = 0; i < text.length(); i++) {
        if (!isVowel(text[i])) {
            result += text[i];
        }
    }


    return result;
}

#endif