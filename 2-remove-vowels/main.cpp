#include <iostream>
#include "ucm_strings.h"
using namespace std;

int main() {

    /*
        Create a function that removes all the vowels from a string.

        Example:
        Welcome to UC Merced => Wlcm t C Mrcd
    */

    string text = "This is a bucket.";
    string result = removeVowels(text);

    cout << result << endl;

    return 0;
}
