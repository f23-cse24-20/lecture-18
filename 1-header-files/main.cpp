#include <iostream>
#include "ucm_banner.h"

using namespace std;

int main() {

    string heading;

    while(getline(cin, heading)) {
        displayBanner(heading);
    }

    return 0;
}